
var btnMedia = document.getElementById("idBtn")

btnMedia.addEventListener("click", function () {


    let nota1 = Number(document.getElementById("idNota1").value)
    let nota2 = Number(document.getElementById("idNota2").value)
    let nota3 = Number(document.getElementById("idNota3").value)
    let nota4 = Number(document.getElementById("idNota4").value)
    let notas = []
    notas.push(nota1, nota2, nota3, nota4)

    //Retirando a maior nota
    let iMaior = 0
    for (let i = 0; i < notas.length; i++) {
        if (notas[i] > notas[iMaior]) {
            iMaior = i
        }
    }

    //Retirando o maior valor inputado
    notas.splice(iMaior, 1)
    console.log(notas);


    //Retirando a menor nota
    let iMenor = 0
    for (let i = 0; i < notas.length; i++) {
        if (notas[i] < notas[iMenor]) {
            iMenor = i
        }
    }

    //Retirando o  menor valor  inputado
    notas.splice(iMenor, 1)
    console.log(notas);


    //Chamando o resultado da média
    calculaMedia(notas)

})

//Realizando o calculo da média
function calculaMedia(array) {

    let resultMedia = 0
    for (let i = 0; i < array.length; i++) {
        resultMedia = resultMedia + array[i]

        //Atribuindo a mensagem de acordo com a média obtida

        if (resultMedia >= 8.5) {
            alert("Conceito A")

        } else if ((resultMedia >= 7) && (resultMedia < 8.5)) {
            alert("Conceito B")

        } else if ((resultMedia >= 5) && (resultMedia < 7)) {
            alert("Conceito C")

        } else {
            alert("Conceito D")
        }
    }
}


